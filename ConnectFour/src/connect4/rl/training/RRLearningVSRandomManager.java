package connect4.rl.training;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import connect4.core.game.GameWindow;
import connect4.core.logic.GameStateConsequence;
import connect4.rl.data.ElasticQTable;
import connect4.rl.data.SAPStateTuple;
import connect4.rl.player.ConnectFourPlayer;
import connect4.rl.player.RandomConnectFourPlayer;
import connect4.rl.player.ReinforcementRecursiveConnectFourPlayer;

/**
 * This is mostly a test
 *
 * @author grovy
 *
 */
public class RRLearningVSRandomManager implements BotGameManager {

	ElasticQTable player1QTable = new ElasticQTable(1);

	private int player1VictoryCount = 0;
	private int player2VictoryCount = 0;

	@Override
	public void runGame() {
		GameWindow gameView;
		ConnectFourPlayer player1;
		ConnectFourPlayer player2;

		JFrame frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 505, 635);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Connect Four - Extremely Random Edition");
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

		boolean playing = true;
		int matchNumber = 1;
		while (playing) {
			gameView = new GameWindow("MULTIPLAYER");
			player1 = new ReinforcementRecursiveConnectFourPlayer(1, gameView, this.player1QTable, 4);
			player2 = new RandomConnectFourPlayer(2, gameView);

			frame.getContentPane().add(gameView, BorderLayout.CENTER);
			frame.revalidate();

			boolean gameOver = false;
			while (!gameOver) {
				SAPStateTuple currentState;
				currentState = player1.processTurn();
				gameOver = processState(player1, player2, currentState);
				if (!gameOver) {
					currentState = player2.processTurn();
					gameOver = processState(player1, player2, currentState);
				}
			}
			if (matchNumber % 100 == 1) {
				System.out.println("Matches: " + matchNumber);
				System.out.println("Player 1 victories: " + this.player1VictoryCount);
				System.out.println("Player 2 victories: " + this.player2VictoryCount);
			}

			matchNumber++;
			if (matchNumber == 450) {
				System.out.println("games!");
			}
//			System.out.println("Game over. Restarting soon...");
			try {
				Thread.sleep(5);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			frame.getContentPane().removeAll();
		}

	}

	private boolean processState(ConnectFourPlayer player1, ConnectFourPlayer player2, SAPStateTuple currentState) {
		boolean player1Victory=false;
		boolean player2Victory=false;
		try {
			player1Victory = player1.endTurn(currentState);
			player2Victory = player2.endTurn(currentState);
		} catch (IllegalStateException e) {
			
			System.out.println();
			throw new IllegalStateException(); 
		}

		if (player1Victory) {
			this.player1VictoryCount++;
		}
		if (player2Victory) {
			this.player2VictoryCount++;
		}

		if ((currentState.getPlayerState().getStateConsequence() == GameStateConsequence.VICTORY)
				|| (currentState.getPlayerState().getStateConsequence() == GameStateConsequence.DRAW)) {
			return true;
		}
		return false;
	}

}
