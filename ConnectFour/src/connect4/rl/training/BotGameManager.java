package connect4.rl.training;

import connect4.rl.player.AbstractConnectFourPlayer;

/**
 * Manages configurable games played by two bots by starting games, ending
 * games, and saving bots.
 *
 * @author grovy
 * @see AbstractConnectFourPlayer
 *
 */
public interface BotGameManager {
	public void runGame();
}
