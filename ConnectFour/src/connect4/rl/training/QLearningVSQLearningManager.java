package connect4.rl.training;

import java.awt.BorderLayout;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

import javax.swing.JFrame;

import connect4.core.game.GameWindow;
import connect4.core.logic.GameStateConsequence;
import connect4.rl.data.ElasticQTable;
import connect4.rl.data.MatchTracker;
import connect4.rl.data.SAPStateTuple;
import connect4.rl.player.ConnectFourPlayer;
import connect4.rl.player.QLearningConnectFourPlayer;
import connect4.rl.player.RuleBasedConnectFourPlayer;

/**
 * This is mostly a test
 *
 * @author grovy
 *
 */
public class QLearningVSQLearningManager implements BotGameManager {

	private ElasticQTable player1QTable = new ElasticQTable(1);
	private ElasticQTable player2QTable = new ElasticQTable(2);

	private MatchTracker tracker = new MatchTracker();

	private HashMap<Integer, Integer> player1ColumnTimesPlayedInAllMatches;
	private HashMap<Integer, Integer> player2ColumnTimesPlayedInAllMatches;

	private int player1VictoryCount = 0;
	private int player2VictoryCount = 0;

	@Override
	public void runGame() {
		this.player1ColumnTimesPlayedInAllMatches = new HashMap<Integer, Integer>();
		this.player2ColumnTimesPlayedInAllMatches = new HashMap<Integer, Integer>();

		GameWindow gameView;
		QLearningConnectFourPlayer player1;
		QLearningConnectFourPlayer player2;

		JFrame frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 505, 635);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Connect Four - swole training room");
		frame.setLocationRelativeTo(null);
		frame.setVisible(false);

		boolean playing = true;
		int matchNumber = 1;
		while (playing) {
			gameView = new GameWindow("MULTIPLAYER");
			player1 = new RuleBasedConnectFourPlayer(1, gameView, this.player1QTable, 0.7);
			player2 = new RuleBasedConnectFourPlayer(2, gameView, this.player2QTable, 0.7);

			frame.getContentPane().add(gameView, BorderLayout.CENTER);
			frame.revalidate();

			boolean gameOver = false;
			while (!gameOver) {
				SAPStateTuple currentState;
				currentState = player1.processTurn();
				gameOver = processState(player1, player2, currentState);
				if (!gameOver) {
					currentState = player2.processTurn();
					gameOver = processState(player1, player2, currentState);
				}
			}
			this.player1ColumnTimesPlayedInAllMatches = this.tracker.updateColumnTimesPlayed(this.player1ColumnTimesPlayedInAllMatches, player1.getColumnPlayCountInOneMatch());
			this.player2ColumnTimesPlayedInAllMatches = this.tracker.updateColumnTimesPlayed(this.player2ColumnTimesPlayedInAllMatches, player2.getColumnPlayCountInOneMatch());
			if (matchNumber % 100 == 1) {
				tracker.printVictoriesAndColumns(matchNumber, this.player1VictoryCount, this.player2VictoryCount, this.player1ColumnTimesPlayedInAllMatches, this.player2ColumnTimesPlayedInAllMatches);
			}
			switch (matchNumber) {
			case 1:
				writeQTableToFile(this.player1QTable, "player1-rulebased-1game-noob.elasQ");
				writeQTableToFile(this.player2QTable, "player2-rulebased-1game-noob.elasQ");
				break;
			case 1000:
				writeQTableToFile(this.player1QTable, "player1-rulebased-1000games-inexperienced.elasQ");
				writeQTableToFile(this.player2QTable, "player2-rulebased-1000games-inexperienced.elasQ");
				break;
			case 10000:
				writeQTableToFile(this.player1QTable, "player1-rulebased-10000games-experienced.elasQ");
				writeQTableToFile(this.player2QTable, "player2-rulebased-10000games-experienced.elasQ");
				break;
			case 50000:
				writeQTableToFile(this.player1QTable, "player1-rulebased-50000games-seasoned.elasQ");
				writeQTableToFile(this.player2QTable, "player2-rulebased-50000games-seasoned.elasQ");
				break;
			case 100000:
				writeQTableToFile(this.player1QTable, "player1-rulebased-100000games-hardcore.elasQ");
				writeQTableToFile(this.player2QTable, "player2-rulebased-100000games-hardcore.elasQ");
				break;
			case 200000:
				writeQTableToFile(this.player1QTable, "player1-rulebased-200000games-super.elasQ");
				writeQTableToFile(this.player2QTable, "player2-rulebased-200000games-super.elasQ");
				System.out.println("Done.");
				return;
			}

			matchNumber++;

			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			frame.getContentPane().removeAll();
		}

	}

	private boolean processState(ConnectFourPlayer player1, ConnectFourPlayer player2, SAPStateTuple currentState) {
		boolean player1Victory = player1.endTurn(currentState);
		boolean player2Victory = player2.endTurn(currentState);

		if (player1Victory) {
			this.player1VictoryCount++;
		}
		if (player2Victory) {
			this.player2VictoryCount++;
		}

		if ((currentState.getPlayerState().getStateConsequence() == GameStateConsequence.VICTORY)
				|| (currentState.getPlayerState().getStateConsequence() == GameStateConsequence.DRAW)) {
			return true;
		}
		return false;
	}

	public void writeQTableToFile(ElasticQTable qTable, String filePath) {
		System.out.println("Saving qTable for " + qTable.getPlayerNumber() + " to disk...");
		try {
			FileOutputStream fileOut = new FileOutputStream(filePath);
			ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
			objectOut.writeObject(qTable);
			objectOut.close();
			System.out.println("qTable for " + qTable.getPlayerNumber() + " has been saved to disk.");

		} catch (Exception ex) {
			System.out.println("qTable for " + qTable.getPlayerNumber() + " has failed to save to disk.");
			ex.printStackTrace();
		}
	}

}
