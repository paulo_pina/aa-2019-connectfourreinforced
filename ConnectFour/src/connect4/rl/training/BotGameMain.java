package connect4.rl.training;

/**
 * Runs configurable games managed by a BotGameManager.
 *
 * @author grovy
 * @see RandomBotGameManager
 *
 */
public class BotGameMain {

	public static void main(String[] args) {
		BotGameManager botGameManager = new QLearningVSQLearningManager();

		botGameManager.runGame();

	}

}
