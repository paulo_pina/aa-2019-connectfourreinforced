package connect4.rl.experimenting;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import connect4.rl.data.ElasticQTable;

public class IntelligentBotGameMain {

	private static final String PLAYER_1_QTABLE_FILE_PATH = "player1-rulebased-200000games-super.elasQ";
	private static final String PLAYER_2_QTABLE_FILE_PATH = "player2-rulebased-200000games-super.elasQ";


	public static void main(String[] args) throws ClassNotFoundException, IOException {
		ElasticQTable player1QT = loadQTableFromFile(PLAYER_1_QTABLE_FILE_PATH);
		ElasticQTable player2QT = loadQTableFromFile(PLAYER_2_QTABLE_FILE_PATH);

		LoadableBotGameManager botGameManager = new IntelligentVSIntelligentManager();
//		LoadableBotGameManager botGameManager = new HumanVSIntelligentManager(2);
		botGameManager.runGame(player1QT, player2QT);

	}

	public static ElasticQTable loadQTableFromFile(String filePath) throws IOException, ClassNotFoundException {
		System.out.println("Loading " + filePath + "...");
		FileInputStream fileInput = new FileInputStream(filePath);
		BufferedInputStream bufferedInput = new BufferedInputStream(fileInput);
		ObjectInputStream objectInput = new ObjectInputStream(bufferedInput);
		ElasticQTable result = (ElasticQTable) objectInput.readObject();

		objectInput.close();
		System.out.println("Finished loading " + filePath + "...");
		return result;
	}

}
