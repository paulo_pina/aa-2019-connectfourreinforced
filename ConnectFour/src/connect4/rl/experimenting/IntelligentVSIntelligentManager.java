package connect4.rl.experimenting;

import java.awt.BorderLayout;
import java.util.HashMap;

import javax.swing.JFrame;

import connect4.core.game.GameWindow;
import connect4.core.logic.GameStateConsequence;
import connect4.rl.data.ElasticQTable;
import connect4.rl.data.MatchTracker;
import connect4.rl.data.SAPStateTuple;
import connect4.rl.player.ConnectFourPlayer;
import connect4.rl.player.QLearningConnectFourPlayer;
import connect4.rl.player.RuleBasedConnectFourPlayer;

public class IntelligentVSIntelligentManager implements LoadableBotGameManager {

	private ElasticQTable player1QTable = new ElasticQTable(1);
	private ElasticQTable player2QTable = new ElasticQTable(2);

	private int player1VictoryCount = 0;
	private int player2VictoryCount = 0;

	private int player1EpsilonCount = 0;
	private int player1UnknownCount = 0;
	private int player1KnownCount = 0;
	private int player1BlockedCount = 0;
	private int player1OpportunityCount = 0;

	private int player2EpsilonCount = 0;
	private int player2UnknownCount = 0;
	private int player2KnownCount = 0;
	private int player2BlockedCount = 0;
	private int player2OpportunityCount = 0;

	private static final long PLAYER_1_TIMEOUT = 0;
	private static final long PLAYER_2_TIMEOUT = 0;
	private static final long GAME_END_TIMEOUT = 0;

	private MatchTracker tracker = new MatchTracker();

	private HashMap<Integer, Integer> player1ColumnTimesPlayedInAllMatches;
	private HashMap<Integer, Integer> player2ColumnTimesPlayedInAllMatches;

	@Override
	public void runGame(ElasticQTable player1QTableConstruct, ElasticQTable player2QTableConstruct) {
		if ((player1QTableConstruct.getPlayerNumber() != this.player1QTable.getPlayerNumber())
				|| (player2QTableConstruct.getPlayerNumber() != this.player2QTable.getPlayerNumber())) {
			System.err.println("WARNING: Unmatching qtable for player. Requested for P1 and P2: "
					+ this.player1QTable.getPlayerNumber() + " and " + this.player2QTable.getPlayerNumber() + "\n"
					+ "Received for P1 and P2: " + player1QTableConstruct.getPlayerNumber() + " and "
					+ player2QTableConstruct.getPlayerNumber());
		}
		this.player1QTable = player1QTableConstruct;
		this.player2QTable = player2QTableConstruct;

		this.player1ColumnTimesPlayedInAllMatches = new HashMap<Integer, Integer>();
		this.player2ColumnTimesPlayedInAllMatches = new HashMap<Integer, Integer>();

		GameWindow gameView;
		ConnectFourPlayer player1;
		ConnectFourPlayer player2;

		JFrame frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 505, 635);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Connect Four - Intelligence experiment");
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

		boolean playing = true;
		int matchNumber = 1;
		while (playing) {
			gameView = new GameWindow("MULTIPLAYER");
			player1 = new RuleBasedConnectFourPlayer(1, gameView, this.player1QTable, 0.01);
			player2 = new RuleBasedConnectFourPlayer(2, gameView, this.player2QTable, 0.01);


			frame.getContentPane().add(gameView, BorderLayout.CENTER);
			frame.revalidate();

			boolean gameOver = false;
			while (!gameOver) {
				SAPStateTuple currentState;
				currentState = player1.processTurn();
				waitMilis(PLAYER_1_TIMEOUT);
				gameOver = processState(player1, player2, currentState);
				if (!gameOver) {
					currentState = player2.processTurn();
					waitMilis(PLAYER_2_TIMEOUT);
					gameOver = processState(player1, player2, currentState);
				}
			}
			this.player1ColumnTimesPlayedInAllMatches = this.tracker.updateColumnTimesPlayed(this.player1ColumnTimesPlayedInAllMatches, ((QLearningConnectFourPlayer) player1).getColumnPlayCountInOneMatch());
			this.player2ColumnTimesPlayedInAllMatches = this.tracker.updateColumnTimesPlayed(this.player2ColumnTimesPlayedInAllMatches, ((QLearningConnectFourPlayer) player2).getColumnPlayCountInOneMatch());
			if (matchNumber % 100 == 1) {
				tracker.printVictoriesAndColumns(matchNumber, this.player1VictoryCount, this.player2VictoryCount, this.player1ColumnTimesPlayedInAllMatches, this.player2ColumnTimesPlayedInAllMatches);

			}
			if (matchNumber == 5002) {
				System.out.println("player,epsilon,unknown,known,opportunity,blocked");
				System.out.println("1,"+this.player1EpsilonCount+","+this.player1UnknownCount+","+this.player1KnownCount+","+this.player1OpportunityCount+","+this.player1BlockedCount);
				System.out.println("2,"+this.player2EpsilonCount+","+this.player2UnknownCount+","+this.player2KnownCount+","+this.player2OpportunityCount+","+this.player2BlockedCount);
				return;
			}
			matchNumber++;

			waitMilis(GAME_END_TIMEOUT);
			frame.getContentPane().removeAll();
		}


	}



	protected void waitMilis(long milis) {
		try {
			Thread.sleep(milis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}



	private boolean processState(ConnectFourPlayer player1, ConnectFourPlayer player2, SAPStateTuple currentState) {
		boolean player1Victory = player1.endTurn(currentState);
		boolean player2Victory = player2.endTurn(currentState);

		if (player1Victory) {
			this.player1VictoryCount++;
		}
		if (player2Victory) {
			this.player2VictoryCount++;
		}

		if (currentState.getPlayerState().getPlayerNumber() == 1) {
			if (player1 instanceof RuleBasedConnectFourPlayer) {
				this.player1BlockedCount += ((RuleBasedConnectFourPlayer) player1).getTimesBlockedThreat();
				this.player1OpportunityCount += ((RuleBasedConnectFourPlayer) player1).getTimesUsedOpportunity();
			}
			if (player1 instanceof QLearningConnectFourPlayer) {
				this.player1EpsilonCount += ((QLearningConnectFourPlayer) player1).getTimesPlayedEpsilon();
				this.player1UnknownCount += ((QLearningConnectFourPlayer) player1).getTimesPlayedUnknown();
				this.player1KnownCount += ((QLearningConnectFourPlayer) player1).getTimesPlayedKnown();
			}
		}

		if (currentState.getPlayerState().getPlayerNumber() == 2) {
			if (player2 instanceof RuleBasedConnectFourPlayer) {
				this.player2BlockedCount += ((RuleBasedConnectFourPlayer) player2).getTimesBlockedThreat();
				this.player2OpportunityCount += ((RuleBasedConnectFourPlayer) player2).getTimesUsedOpportunity();
			}
			if (player2 instanceof QLearningConnectFourPlayer) {
				this.player2EpsilonCount += ((QLearningConnectFourPlayer) player2).getTimesPlayedEpsilon();
				this.player2UnknownCount += ((QLearningConnectFourPlayer) player2).getTimesPlayedUnknown();
				this.player2KnownCount += ((QLearningConnectFourPlayer) player2).getTimesPlayedKnown();
			}
		}
		if ((currentState.getPlayerState().getStateConsequence() == GameStateConsequence.VICTORY)
				|| (currentState.getPlayerState().getStateConsequence() == GameStateConsequence.DRAW)) {
			return true;
		}
		return false;
	}

}
