package connect4.rl.experimenting;

import connect4.rl.data.ElasticQTable;
import connect4.rl.player.AbstractConnectFourPlayer;

/**
 * Manages configurable games played by two bots by starting games, ending
 * games, and saving/loading bots.
 *
 * @author grovy
 * @see AbstractConnectFourPlayer
 *
 */
public interface LoadableBotGameManager {
	public void runGame(ElasticQTable player1QTable, ElasticQTable player2QTable);
}
