package connect4.rl.experimenting;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import connect4.core.game.GameWindow;
import connect4.core.logic.GameStateConsequence;
import connect4.rl.data.ElasticQTable;
import connect4.rl.data.SAPStateTuple;
import connect4.rl.player.ConnectFourPlayer;
import connect4.rl.player.HumanConnectFourPlayer;
import connect4.rl.player.RuleBasedConnectFourPlayer;

public class HumanVSIntelligentManager implements LoadableBotGameManager {

	private ElasticQTable player1QTable = new ElasticQTable(1);
	private ElasticQTable player2QTable = new ElasticQTable(2);
	private final int humanPlayerSlot;

	public HumanVSIntelligentManager(int humanPlayerSlot) {
		this.humanPlayerSlot = humanPlayerSlot;
	}

	@Override
	public void runGame(ElasticQTable player1QTableConstruct, ElasticQTable player2QTableConstruct) {
		if ((player1QTableConstruct.getPlayerNumber() != this.player1QTable.getPlayerNumber())
				|| (player2QTableConstruct.getPlayerNumber() != this.player2QTable.getPlayerNumber())) {
			System.err.println("WARNING: Unmatching qtable for player. Requested for P1 and P2: "
					+ this.player1QTable.getPlayerNumber() + " and " + this.player2QTable.getPlayerNumber() + "\n"
					+ "Received for P1 and P2: " + player1QTableConstruct.getPlayerNumber() + " and "
					+ player2QTableConstruct.getPlayerNumber());
		}
		this.player1QTable = player1QTableConstruct;
		this.player2QTable = player2QTableConstruct;

		GameWindow gameView;
		ConnectFourPlayer player1;
		ConnectFourPlayer player2;

		JFrame frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 505, 635);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Connect Four - Human vs Bot");
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		frame.setAlwaysOnTop(true);

		boolean playing = true;
		while (playing) {
			gameView = new GameWindow("MULTIPLAYER");
			switch (this.humanPlayerSlot) {
			case 1:
				player1 = new HumanConnectFourPlayer(1, gameView);
				player2 = new RuleBasedConnectFourPlayer(2, gameView, this.player2QTable, 0.01);
				break;
			case 2:
				player1 = new RuleBasedConnectFourPlayer(1, gameView, this.player1QTable, 0.01);
				player2 = new HumanConnectFourPlayer(2, gameView);
				break;
			default:
				throw new IllegalStateException("uh-oh invalid player slot");
			}

			frame.getContentPane().add(gameView, BorderLayout.CENTER);
			frame.revalidate();

			boolean gameOver = false;
			while (!gameOver) {
				SAPStateTuple currentState;
				currentState = player1.processTurn();
				gameOver = processState(player1, player2, currentState);
				if (!gameOver) {
					currentState = player2.processTurn();
					gameOver = processState(player1, player2, currentState);
				}
			}
			waitMilis(5000);
			frame.getContentPane().removeAll();
		}
	}

	private void waitMilis(long milis) {
		try {
			Thread.sleep(milis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private boolean processState(ConnectFourPlayer player1, ConnectFourPlayer player2, SAPStateTuple currentState) {
		player1.endTurn(currentState);
		player2.endTurn(currentState);

		if ((currentState.getPlayerState().getStateConsequence() == GameStateConsequence.VICTORY)
				|| (currentState.getPlayerState().getStateConsequence() == GameStateConsequence.DRAW)) {
			return true;
		}
		return false;
	}

}
