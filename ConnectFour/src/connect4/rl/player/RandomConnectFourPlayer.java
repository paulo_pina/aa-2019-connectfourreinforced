package connect4.rl.player;

import java.util.ArrayList;
import java.util.Random;

import connect4.core.game.GameWindow;
import connect4.rl.data.SAPStateTuple;

public class RandomConnectFourPlayer extends AbstractConnectFourPlayer {

	private static final long serialVersionUID = -7597718013890894235L;
	private Random r = new Random();

	public RandomConnectFourPlayer(int playerNumber, GameWindow gameView) {
		super(playerNumber, gameView);
	}

	@Override
	protected int makeMove(ArrayList<Integer> freeColumns) {
		int randomColumnIndex = r.nextInt(freeColumns.size());
		int randomColumn = freeColumns.get(randomColumnIndex);
		return randomColumn;
	}

	@Override
	protected void reactToLoss(SAPStateTuple endState) {
//		System.out.println("Player " + this.getPlayerNumber() + " acknowledges that it lost the game on turn "
//				+ super.getGameView().getTurnCount() + ".");
	}

	@Override
	protected void reactToDraw(SAPStateTuple endState) {
//		System.out.println("Player " + this.getPlayerNumber()
//				+ " acknowledges that both players are a good match for each other on turn "
//				+ super.getGameView().getTurnCount() + ".");
	}

	@Override
	protected void reactToVictory(SAPStateTuple endState) {
//		System.out.println("Player " + this.getPlayerNumber() + " acknowledges that it won the game on turn "
//				+ super.getGameView().getTurnCount() + ".");
	}

	@Override
	protected void reactToContinue(SAPStateTuple lastState) {
		; // do nothing. this bot doesn't give a damn. it'll play randomly all the time!
	}

}
