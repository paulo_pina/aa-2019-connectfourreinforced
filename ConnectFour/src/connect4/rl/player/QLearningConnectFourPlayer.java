package connect4.rl.player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Random;

import connect4.core.game.GameWindow;
import connect4.core.logic.GameStateConsequence;
import connect4.core.logic.StateChecker;
import connect4.rl.data.ElasticQTable;
import connect4.rl.data.SAPStateTuple;

public class QLearningConnectFourPlayer extends AbstractConnectFourPlayer {

//	private enum Polarity {
//		FOCUS_ON_START,
//		FOCUS_ON_END,
//		FOCUS_ON_BOTH_ENDS,
//		FOCUS_ON_MIDGAME
//	} TODO future work: Maybe make a bot that has polarity.

	private static final long serialVersionUID = 5120821495375942521L;
	private ElasticQTable qTable;
	private LinkedList<SAPStateTuple> stateActionsWitnessed;
	private Random rng = new Random();

	private HashMap<Integer, Integer> columnTimesPlayedInOneMatch = new HashMap<Integer, Integer>();

	/**
	 * Factor of exploration. epsilon% of the time, the bot will decide to take a
	 * random action.<br>
	 * A high epsilon% is probably good for new qTables, but not very good for
	 * mature qTables.
	 */
	private final double epsilon;

	/**
	 * Learning factor or learning rate. alpha% of the reward will be administered
	 * upon a reaction.<br>
	 * A high alpha% means that new experiences are way more important than older
	 * ones.
	 */
	private final double alpha = 0.6;

	/**
	 * Discount factor. Balances the amount of reward that we really want to give
	 * relative to future potential rewards.
	 */
	private final double gamma = 0.81;

	private int timesPlayedEpsilon = 0;
	private int timesPlayedUnknown = 0;
	private int timesPlayedKnown = 0;

//	/**
//	 * Decides on what actions are important depending on what
//	 */
//	private final Polarity polarity = Polarity.FOCUS_ON_START;

	public QLearningConnectFourPlayer(int playerNumber, GameWindow gameView, double epsilon) {
		super(playerNumber, gameView);
		this.qTable = new ElasticQTable(playerNumber);
		this.stateActionsWitnessed = new LinkedList<SAPStateTuple>();
		this.epsilon = epsilon;
	}

	public QLearningConnectFourPlayer(int playerNumber, GameWindow gameView, ElasticQTable qTable, double epsilon) {
		super(playerNumber, gameView);
		if (qTable.getPlayerNumber() != playerNumber) {
			System.err.println("WARNING: qTable and player number do not match: " + playerNumber + " player vs qTable"
					+ qTable.getPlayerNumber());
		}
		this.qTable = qTable;
		this.stateActionsWitnessed = new LinkedList<SAPStateTuple>();
		this.epsilon = epsilon;
	}

	/**
	 * Returns the bot's qTable
	 *
	 * @return ElasticQTable instance
	 */
	public ElasticQTable getQTable() {
		return this.qTable;
	}

	/**
	 * Returns the actions that were witnessed by this bot in order of appearance.
	 *
	 * @return SAPStateTuples in a LinkedList
	 */
	public LinkedList<SAPStateTuple> getStateActionsWitnessed() {
		return stateActionsWitnessed;
	}

	@Override
	protected int makeMove(ArrayList<Integer> freeColumns) {
		// I'll always want to calculate a random column, just in case. I may not have a
		// good qTable yet.
		int randomColumnIndex = this.rng.nextInt(freeColumns.size());
		int randomColumn = freeColumns.get(randomColumnIndex);

		// decide whether I want to make a random move or an educated one. If it's
		// random, then return what I've done earlier.
		if (this.rng.nextDouble() < this.epsilon) {
			//System.out.println("QLCFP plays on random column (epsilon):" + randomColumn);

			this.timesPlayedEpsilon++;
			updateTimesPlayed(randomColumn);
			return randomColumn;
		}

		// this will be an educated pick, if I can actually grab something off of the qTable.
		SAPStateTuple currentState = super.getGameView().getLastWitnessedMove();
		if (currentState != null) { // chained if or else it dies
			if (currentState.getStateActionPair().getPlayerNumber() == this.getPlayerNumber()) {
				throw new IllegalStateException(
						"ERROR: Apparently the current state has myself as the culprit?? Number: "
								+ this.getPlayerNumber());
			}
		}
		int zeroes = 0;
		int potentialColumn = 0;
		Double maxQuality = Double.NEGATIVE_INFINITY;

		int[][] currentMatrix = this.getGameView().getCurrentMatrix();
		for (Integer column: freeColumns) {
			// build the next state...
			SAPStateTuple nextState = buildSAPStateTuple(currentMatrix, this.getPlayerNumber(), column);

			// query the qtable...
			double nextQuality = this.qTable.getWeight(nextState);
			if (nextQuality == 0.0) {
				zeroes++;
			}
			if (maxQuality < nextQuality) {
				maxQuality = nextQuality;
				potentialColumn = column;
			}
		}
		if (zeroes == freeColumns.size()) {
			//System.out.println("QLCFP plays on random column (no solution): " + randomColumn + "; zeroes: " + zeroes);

			this.timesPlayedUnknown++;
			updateTimesPlayed(randomColumn);
			return randomColumn;
		}
		else {
			//System.err.println("QLCFP " + this.getPlayerNumber() + " plays on potential column (have a solution): " + potentialColumn);
			this.timesPlayedKnown++;
			updateTimesPlayed(potentialColumn);
			return potentialColumn;
		}
	}

	protected void updateTimesPlayed(int columnNumber) {
		int currentTimesPlayed = 0;
		if (this.columnTimesPlayedInOneMatch.get(columnNumber) != null) {
			currentTimesPlayed = this.columnTimesPlayedInOneMatch.get(columnNumber);
		}
		currentTimesPlayed = currentTimesPlayed + 1;
		this.columnTimesPlayedInOneMatch.put(columnNumber, currentTimesPlayed);
	}

	public HashMap<Integer, Integer> getColumnPlayCountInOneMatch() {
		return this.columnTimesPlayedInOneMatch;
	}

	@Override
	protected void reactToLoss(SAPStateTuple endState) {
		// Losses are really bad and should be avoided at any cost. The pairs that led
		// up to this defeat should have their weights reduced.
		stateActionsWitnessed.add(endState);
		administerReward(-5.0);
//		System.out.println("Player QLCFP " + this.getPlayerNumber() + " acknowledges that it lost the game on turn "
//				+ super.getGameView().getTurnCount() + ".");
	}

	@Override
	protected void reactToDraw(SAPStateTuple endState) {
		// We don't like draws, but they're better than losses. No changes to
		// weights, but non-existant pair-weight tuples will be inserted as 0.
		stateActionsWitnessed.add(endState);
		administerReward(0.0);
//		System.out.println("Player QLCFP " + this.getPlayerNumber()
//				+ " acknowledges that both players are a good match for each other on turn "
//				+ super.getGameView().getTurnCount() + ".");

	}

	@Override
	protected void reactToVictory(SAPStateTuple endState) {
		// We like victories and they must be encouraged. The pairs that led up
		// to this victory should have their weights increased.
		stateActionsWitnessed.add(endState);
		administerReward(5.0);
//		System.out.println("Player QLCFP " + this.getPlayerNumber() + " acknowledges that it won the game on turn "
//				+ super.getGameView().getTurnCount() + ".");
	}

	/**
	 * Administers a reward to all of the state-action state tuples that were
	 * witnessed during the game.
	 *
	 * @param reward
	 */
	private void administerReward(double reward) {
		double rewardImportanceFactor = 0.5;
		double rewardImportanceAddend = 0.1;
		for (SAPStateTuple sapTuple : this.stateActionsWitnessed) {
			double currentWeight = this.qTable.getWeight(sapTuple);
			double maxQualityAction = calculateMaxQualityActionFromNewState(sapTuple);
			currentWeight = currentWeight + (this.alpha * (rewardImportanceFactor * reward + (this.gamma * maxQualityAction) - currentWeight));
			rewardImportanceFactor = rewardImportanceFactor + rewardImportanceAddend;
			this.qTable.putWeight(sapTuple, currentWeight);
		}

//		System.out.println("===");
//		System.out.println("Times played epsilon: " + this.timesPlayedEpsilon);
//		System.out.println("Times played unknown: " + this.timesPlayedUnknown);
//		System.out.println("Times played   known: " + this.timesPlayedKnown);
//		System.out.println("===");
	}

	/**
	 * Grabs the maximum possible reward from the future state's actions.
	 *
	 * @param currentSap
	 * @return reward
	 */
	private double calculateMaxQualityActionFromNewState(SAPStateTuple currentSap) {
		if (currentSap.getPlayerState().getStateConsequence() != GameStateConsequence.CONTINUE) {
			return 0.0; // There won't be a new state if the game's stopped or if an illegal move was
						// made.
		}

		// we need to build proper SAPStateTuples for the next state and the actions.
		int[][] nextMatrix = currentSap.getStateActionPair().getFutureState();
		int differentPlayerNumber = -1;

		if (currentSap.getPlayerState().getPlayerNumber() == 1) {
			differentPlayerNumber = 2;
		} else if (currentSap.getPlayerState().getPlayerNumber() == 2) {
			differentPlayerNumber = 1;
		}

		if (differentPlayerNumber == -1) {
			throw new IllegalStateException(
					"ERROR: Player number for max quality action from new state is bad: " + differentPlayerNumber);
		}

		Double maxQuality = Double.NEGATIVE_INFINITY;
		ArrayList<Integer> freeColumns = StateChecker.getFreeColumns(nextMatrix);
		for (Integer action : freeColumns) {
			SAPStateTuple futureSapTuple = buildSAPStateTuple(nextMatrix, differentPlayerNumber, action);

			double futureQuality = this.qTable.getWeight(futureSapTuple);
			if (futureQuality > maxQuality) {
				maxQuality = futureQuality;
			}
		}

		return maxQuality;
	}

	@Override
	protected void reactToContinue(SAPStateTuple lastState) {
		// Continues are perfectly neutral and don't affect the qTable at all. Just
		// add the state to the witnessed states.
		stateActionsWitnessed.add(lastState);
	}

	public int getTimesPlayedEpsilon() {
		return timesPlayedEpsilon;
	}

	public int getTimesPlayedUnknown() {
		return timesPlayedUnknown;
	}

	public int getTimesPlayedKnown() {
		return timesPlayedKnown;
	}

}
