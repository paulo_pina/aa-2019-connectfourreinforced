package connect4.rl.player;

import java.util.ArrayList;
import java.util.HashMap;

import connect4.core.game.GameWindow;
import connect4.core.logic.GameStateConsequence;
import connect4.core.logic.PlayerState;
import connect4.core.logic.StateChecker;
import connect4.rl.data.SAPStateTuple;
import connect4.rl.data.StateActionPair;

/**
 * Defines a basic ConnectFourPlayer without any machine learning
 * implementations.
 *
 * @author grovy
 *
 */
public abstract class AbstractConnectFourPlayer implements ConnectFourPlayer {

	private static final long serialVersionUID = 7440250680532950354L;

	private int playerNumber;
	private int adversaryNumber;
	private GameWindow gameView;
	private HashMap<Integer, String> columnMap;

	public AbstractConnectFourPlayer(int playerNumber, GameWindow gameView) {
		validatePlayerNumber(playerNumber);
		this.playerNumber = playerNumber;

		if (this.playerNumber == 1) {
			this.adversaryNumber = 2;
		} else {
			this.adversaryNumber = 1;
		}

		this.gameView = gameView;

		this.columnMap = new HashMap<Integer, String>();
		initializeColumnMap();
	}

	private void initializeColumnMap() {
		columnMap.put(0, "A");
		columnMap.put(1, "B");
		columnMap.put(2, "C");
		columnMap.put(3, "D");
		columnMap.put(4, "E");
		columnMap.put(5, "F");
		columnMap.put(6, "G");
	}

	private void validatePlayerNumber(int playerNumber) {
		if ((playerNumber != 1) && (playerNumber != 2)) {
			throw new IllegalArgumentException("ERROR: Invalid player number on player creation: " + playerNumber);
		}
	}

	@Override
	public int getPlayerNumber() {
		return this.playerNumber;
	}

	@Override
	public GameWindow getGameView() {
		return this.gameView;
	}

	@Override
	public int getAdversaryNumber() {
		return this.adversaryNumber;
	}

	@Override
	public boolean endTurn(SAPStateTuple endState) {
		if (endState.getPlayerState().getStateConsequence() == GameStateConsequence.DRAW) {
			reactToDraw(endState);
			return false;
		} else if (endState.getPlayerState().getStateConsequence() == GameStateConsequence.VICTORY
				&& this.getPlayerNumber() == endState.getPlayerState().getPlayerNumber()) {
			reactToVictory(endState);
			return true;
		} else if (endState.getPlayerState().getStateConsequence() == GameStateConsequence.VICTORY
				&& this.getPlayerNumber() != endState.getPlayerState().getPlayerNumber()) {
			reactToLoss(endState);
			return false;
		} else if (endState.getPlayerState().getStateConsequence() == GameStateConsequence.CONTINUE) {
			reactToContinue(endState);
			return false;
		}
		else {
			throw new IllegalStateException("Something went wrong. Player " + this.getPlayerNumber()
					+ " notified of game end but the game's not over.");
		}
	}

	public String getColumnString(int key) {
		return this.columnMap.get(key);
	}

	@Override
	public SAPStateTuple processTurn() {
		GameWindow currentView = this.getGameView();
		int[][] currentMatrix = currentView.getCurrentMatrix();
		ArrayList<Integer> freeColumns = StateChecker.getFreeColumns(currentMatrix);

		int column = makeMove(freeColumns);

		return currentView.addPieceToColumn(this.getColumnString(column));
	}

	/**
	 * Defines behaviour on losing the game. Please don't make a bot that destroys
	 * the universe if it loses.
	 *
	 * @param endState
	 */
	protected abstract void reactToLoss(SAPStateTuple endState);

	/**
	 * Defines behaviour on reacting to a draw.
	 *
	 * @param endState
	 */
	protected abstract void reactToDraw(SAPStateTuple endState);

	/**
	 * Defines behaviour on reacting to a victory. Please don't make the bot talk
	 * trash if it wins.
	 *
	 * @param endState
	 */
	protected abstract void reactToVictory(SAPStateTuple endState);

	/**
	 * Defines behaviour on reacting to a continue.
	 *
	 * @param lastState
	 */
	protected abstract void reactToContinue(SAPStateTuple lastState);

	/**
	 * Defines behaviour on making a move, returning the column number on which it
	 * decided to play on.
	 *
	 * @param freeColumns
	 * @return
	 */
	protected abstract int makeMove(ArrayList<Integer> freeColumns);

	/**
	 * @param nextMatrix
	 * @param playerNumber
	 * @param action
	 * @return
	 */
	protected SAPStateTuple buildSAPStateTuple(int[][] nextMatrix, int playerNumber, Integer action) {
		if ((playerNumber != 1) && (playerNumber != 2)) {
			throw new IllegalArgumentException("ERROR: bad player number: " + playerNumber);
		}
		Enum<GameStateConsequence> stateConsq = StateChecker.verifyGameState(playerNumber, nextMatrix);
		StateActionPair futureSap = new StateActionPair(nextMatrix, action, playerNumber);
		PlayerState futurePs = new PlayerState(stateConsq, playerNumber);
		SAPStateTuple futureSapTuple = new SAPStateTuple(futureSap, futurePs);
		return futureSapTuple;
	}

	public int getThreatenedColumn(GameWindow gameView, ArrayList<Integer> freeColumns, int playerNumber) {
		for (Integer column: freeColumns) {
			int[][] futureMatrix = StateChecker.getNextMatrix(gameView.getCurrentMatrix(), column, playerNumber);
			SAPStateTuple futureState = this.buildSAPStateTuple(futureMatrix, playerNumber, column);
			if (futureState.getPlayerState().getStateConsequence() == GameStateConsequence.VICTORY) {
				return column;
			}
		}
		return -1;
	}

	public int getWinningColumn(GameWindow gameView, ArrayList<Integer> freeColumns, int playerNumber) {
		for (Integer column: freeColumns) {
			int[][] futureMatrix = StateChecker.getNextMatrix(gameView.getCurrentMatrix(), column, playerNumber);
			SAPStateTuple futureState = this.buildSAPStateTuple(futureMatrix, playerNumber, column);
			if (futureState.getPlayerState().getStateConsequence() == GameStateConsequence.VICTORY) {
				return column;
			}
		}
		return -1;
	}

}
