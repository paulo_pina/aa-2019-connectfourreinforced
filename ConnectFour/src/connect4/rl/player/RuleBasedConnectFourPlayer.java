package connect4.rl.player;

import java.util.ArrayList;

import connect4.core.game.GameWindow;
import connect4.rl.data.ElasticQTable;

public class RuleBasedConnectFourPlayer extends QLearningConnectFourPlayer {

	/**
	 *
	 */
	private static final long serialVersionUID = 141470622033107422L;

	private int timesBlockedThreat = 0;
	private int timesUsedOpportunity = 0;

	public RuleBasedConnectFourPlayer(int playerNumber, GameWindow gameView, double epsilon) {
		super(playerNumber, gameView, epsilon);
	}

	public RuleBasedConnectFourPlayer(int playerNumber, GameWindow gameView, ElasticQTable qTable, double epsilon) {
		super(playerNumber, gameView, qTable, epsilon);
	}

	@Override
	protected int makeMove(ArrayList<Integer> freeColumns) {
		int winningColumn = getWinningColumn(super.getGameView(), freeColumns, this.getPlayerNumber());
		if (winningColumn > -1 && winningColumn < 7) {
			this.timesUsedOpportunity++;
			return winningColumn;
		}

		int threatenedColumn = getThreatenedColumn(super.getGameView(), freeColumns, this.getAdversaryNumber());
		if (threatenedColumn > -1 && threatenedColumn < 7) {
			this.timesBlockedThreat++;
			return threatenedColumn;
		}

		return super.makeMove(freeColumns);
	}

	public int getTimesBlockedThreat() {
		return timesBlockedThreat;
	}

	public int getTimesUsedOpportunity() {
		return timesUsedOpportunity;
	}


}
