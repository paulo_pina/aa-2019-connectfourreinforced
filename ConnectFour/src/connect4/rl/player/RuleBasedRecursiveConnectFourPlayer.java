package connect4.rl.player;

import java.util.ArrayList;

import connect4.core.game.GameWindow;
import connect4.rl.data.ElasticQTable;

public class RuleBasedRecursiveConnectFourPlayer extends ReinforcementRecursiveConnectFourPlayer {


	/**
	 *
	 */
	private static final long serialVersionUID = 9092901221464614474L;

	public RuleBasedRecursiveConnectFourPlayer(int playerNumber, GameWindow gameView,
			int levelRegression) {
		super(playerNumber, gameView, levelRegression);
		// TODO Auto-generated constructor stub
	}

	public RuleBasedRecursiveConnectFourPlayer(int playerNumber, GameWindow gameView, ElasticQTable qTable,
			int levelRegression) {
		super(playerNumber, gameView, qTable, levelRegression);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected int makeMove(ArrayList<Integer> freeColumns) {
		int winningColumn = getWinningColumn(super.getGameView(), freeColumns, this.getPlayerNumber());
		if (winningColumn > -1 && winningColumn < 7) {
			return winningColumn;
		}

		int threatenedColumn = getThreatenedColumn(super.getGameView(), freeColumns, this.getAdversaryNumber());
		if (threatenedColumn > -1 && threatenedColumn < 7) {
			return threatenedColumn;
		}

		return super.makeMove(freeColumns);
	}


}
