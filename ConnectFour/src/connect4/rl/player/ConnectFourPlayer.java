package connect4.rl.player;

import java.io.Serializable;

import connect4.core.game.GameWindow;
import connect4.rl.data.SAPStateTuple;

/**
 * Defines a Connect Four player. A player needs to know if it's their turn, and
 * needs to be able to tell others about its player number in order to play the
 * game.
 *
 * @author grovy
 *
 */
public interface ConnectFourPlayer extends Serializable {


	/**
	 * Tells the ConnectFourPlayer that it's its turn. When the ConnectFourPlayer
	 * knows that it's its turn, it'll play on some column, assuming that it can do
	 * so.
	 * @return the state acquired from the game view
	 */
	public SAPStateTuple processTurn();

	/**
	 * Gets the player's assigned number. It should be 1 or 2.
	 */
	public int getPlayerNumber();

	/**
	 * Gets the player adversary's assigned number. It should be different from this
	 * player's player number and be 1 or 2.
	 */
	public int getAdversaryNumber();

	/**
	 * Gets the player's current GameWindow.
	 */
	public GameWindow getGameView();

	/**
	 * Tells the ConnectFourPlayer that their turn is over and how it ended. Game
	 * halting turns should stop the bot on the match manager.
	 *
	 * @param endState
	 * @return true if it won on that turn, false if it didn't
	 */
	public boolean endTurn(SAPStateTuple endState);

}
