package connect4.rl.player;

import java.util.ArrayList;
import java.util.Scanner;

import connect4.core.game.GameWindow;
import connect4.rl.data.SAPStateTuple;

public class HumanConnectFourPlayer extends AbstractConnectFourPlayer {

	/**
	 *
	 */
	private static final long serialVersionUID = 3000789853976866224L;
	private Scanner keyboardIn = new Scanner(System.in);

	public HumanConnectFourPlayer(int playerNumber, GameWindow gameView) {
		super(playerNumber, gameView);
	}

	@Override
	protected void reactToLoss(SAPStateTuple endState) {
		;

	}

	@Override
	protected void reactToDraw(SAPStateTuple endState) {
		;

	}

	@Override
	protected void reactToVictory(SAPStateTuple endState) {
		;

	}

	@Override
	protected void reactToContinue(SAPStateTuple lastState) {
		;
	}

	@Override
	protected int makeMove(ArrayList<Integer> freeColumns) {
		boolean validMove = false;
		int column = -1;
		System.out.print("Enter the column that you want to play: ");
		while (!validMove) {
			String input = this.keyboardIn.next().trim().toLowerCase();
			int inputNumber = -1;
			try {
				inputNumber = Integer.parseInt(input);
			} catch (NumberFormatException e) {
				switch (input) {
				case "a":
					inputNumber = 0;
					break;
				case "b":
					inputNumber = 1;
					break;
				case "c":
					inputNumber = 2;
					break;
				case "d":
					inputNumber = 3;
					break;
				case "e":
					inputNumber = 4;
					break;
				case "f":
					inputNumber = 5;
					break;
				case "g":
					inputNumber = 6;
					break;
				}
			}
			if (!freeColumns.contains(inputNumber)) {
				System.out.print("\nThat column is invalid. Try another one: ");
				continue;
			} else {
				column = inputNumber;
				validMove = true;
			}
		}

		if (column < 0 || column > 6) {
			throw new IllegalStateException(
					"ERROR: Invalid column number by player" + this.getPlayerNumber() + ": " + column);
		}
		return column;
	}

}
