package connect4.rl.player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import connect4.core.game.GameWindow;
import connect4.core.logic.GameStateConsequence;
import connect4.core.logic.PlayerState;
import connect4.core.logic.StateChecker;
import connect4.rl.data.ElasticQTable;
import connect4.rl.data.SAPStateTuple;
import connect4.rl.data.StateActionPair;

public class ReinforcementRecursiveConnectFourPlayer extends QLearningConnectFourPlayer {

	/**
	 *
	 */
	private static final long serialVersionUID = 8539405236056310484L;

	private Random random = new Random();
	private int levelRegression;
	private int levelCounterRegression;
	private HashMap<String, Double> stateResults;
	private int columnToGame;
	private String wayToWin = "";

	public ReinforcementRecursiveConnectFourPlayer(int playerNumber, GameWindow gameView, int levelRegression) {
		super(playerNumber, gameView, 0.1);
		this.levelRegression = levelRegression;
	}

	public ReinforcementRecursiveConnectFourPlayer(int playerNumber, GameWindow gameView, ElasticQTable qTable,
			int levelRegression) {
		super(playerNumber, gameView, qTable, 0.1);
		this.levelRegression = levelRegression;
	}

	@Override
	protected int makeMove(ArrayList<Integer> freeColumns) {
		SAPStateTuple currentState = super.getGameView().getLastWitnessedMove();
		if (currentState != null) { // chained if or else it dies
			if (currentState.getStateActionPair().getPlayerNumber() == this.getPlayerNumber()) {
				throw new IllegalStateException(
						"ERROR: Apparently the current state has myself as the culprit?? Number: "
								+ this.getPlayerNumber());
			}
		}
		int[][] matrix = new int[6][7];
		int playerNumber = 1;
		if (currentState != null) {
			matrix = currentState.getStateActionPair().getFutureState();
			playerNumber = currentState.getStateActionPair().getPlayerNumber();
		}
		String key = "";
		double value = Double.NEGATIVE_INFINITY;

		levelCounterRegression = 1;
		stateResults = new HashMap<String, Double>();
		recursiveProcess(matrix, playerNumber);

		for (Map.Entry<String, Double> entry : stateResults.entrySet()) {
		    if (entry.getValue() > value) {
		    	key = entry.getKey().substring(0, entry.getKey().indexOf("-"));
		    	value = entry.getValue();
		    }
		    else if (entry.getValue() == 0){
		    	int randomColumnIndex = this.random.nextInt(freeColumns.size());
				int randomColumn = freeColumns.get(randomColumnIndex);
		    	key = String.valueOf(randomColumn);
		    	break;
		    }
		}

		if(stateResults.isEmpty()) {
			int randomColumnIndex = this.random.nextInt(freeColumns.size());
			int randomColumn = freeColumns.get(randomColumnIndex);
	    	key = String.valueOf(randomColumn);
		}

	    try {
    		columnToGame = Integer.parseInt(key);
    	} catch(NumberFormatException e) {

    	}
	    super.updateTimesPlayed(columnToGame);
		return columnToGame;
	}

	private void recursiveProcess(int[][] matrix, int playerNumber) {
		for (int actionNumber = 0; actionNumber < 7; actionNumber++) {
			boolean i = true;
			if (!StateChecker.isColumnFull(actionNumber, matrix)) {
				Enum<GameStateConsequence> getStateConsequence = StateChecker.verifyGameState(playerNumber, matrix);
				StateActionPair stateActionPair = new StateActionPair(matrix, actionNumber, playerNumber);
				PlayerState playerState = new PlayerState(getStateConsequence, playerNumber);
				SAPStateTuple sapStateTuple = new SAPStateTuple(stateActionPair, playerState);

				wayToWin = wayToWin + actionNumber + "-";

				if (levelCounterRegression < levelRegression) {
					levelCounterRegression++;
					recursiveProcess(stateActionPair.getFutureState(), stateActionPair.getPlayerNumber());
					wayToWin = wayToWin.substring(0, wayToWin.lastIndexOf("-") - 1);
					i = false;
				}

				if(i) {
					stateResults.put(wayToWin, super.getQTable().getWeight(sapStateTuple));
					wayToWin = wayToWin.substring(0, wayToWin.lastIndexOf("-") - 1);
				}
			}
		}
		levelCounterRegression--;
		return;
	}
}
