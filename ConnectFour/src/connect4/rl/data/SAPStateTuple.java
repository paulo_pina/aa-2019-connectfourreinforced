package connect4.rl.data;

import java.io.Serializable;
import java.util.Arrays;

import connect4.core.logic.PlayerState;

/**
 * Links a StateActionPair instance and a PlayerState instance together.
 *
 * @author grovy
 *
 */
public final class SAPStateTuple implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 8883091323339410213L;
	private final StateActionPair stateActionPair;
	private final PlayerState playerState;

	public SAPStateTuple(StateActionPair sap, PlayerState ps) {
		if (sap.getPlayerNumber() != ps.getPlayerNumber()) {
			throw new IllegalStateException("ERROR: How the hell is a SAP player number different from a PS number? "
					+ sap.getPlayerNumber() + " SAP vs PS " + ps.getPlayerNumber());
		}

		this.stateActionPair = sap;
		this.playerState = ps;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int[][] matrix = stateActionPair.getState();
		int action = stateActionPair.getAction();
		int result = prime * Arrays.hashCode(matrix[0]) * Arrays.hashCode(matrix[1]) * Arrays.hashCode(matrix[2])
				* Arrays.hashCode(matrix[3]) * Arrays.hashCode(matrix[4]) * Arrays.hashCode(matrix[5]) + action;

		return result;
	}

	@Override
	public boolean equals(Object anotherTuple) {
		if (!(anotherTuple instanceof SAPStateTuple)) {
			return false;
		}
		SAPStateTuple actualTuple = (SAPStateTuple) anotherTuple;
		if (this == actualTuple) {
			return true;
		}
		if ((this.stateActionPair == actualTuple.getStateActionPair())
				&& (this.playerState == actualTuple.getPlayerState())) {
			return true;
		}
		if (this.stateActionPair.equals(actualTuple.getStateActionPair())
				&& this.playerState.equals(actualTuple.getPlayerState())) {
			return true;
		}

		return false;
	}

	/**
	 * Returns the State Action Pair associated with a Player State.
	 *
	 * @return StateActionPair instance
	 */
	public StateActionPair getStateActionPair() {
		return stateActionPair;
	}

	/**
	 * Returns the Player State associated with a State Action Pair.
	 *
	 * @return PlayerState instance.
	 */
	public PlayerState getPlayerState() {
		return playerState;
	}
}
