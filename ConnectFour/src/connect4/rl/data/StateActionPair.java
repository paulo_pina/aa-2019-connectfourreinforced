package connect4.rl.data;

import java.io.Serializable;
import java.util.Arrays;

import connect4.core.logic.StateChecker;

/**
 * Defines a pair of state and action. A state is defined by the current game
 * matrix, the action is the column played afterwards and the player that
 * performed that action.
 *
 * @author grovy
 *
 */
public final class StateActionPair implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -8260618012490859532L;
	private final int[][] state;
	private final int[][] futureState;
	private final int action;
	private final int playerNumber;

	public StateActionPair(int[][] state, int action, int playerNumber) {
		int[][] result = new int[6][7];
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 7; j++) {
				result[i][j] = state[i][j];
			}
		}

		this.state = result;
		this.action = action;
		this.playerNumber = playerNumber;
		this.futureState = StateChecker.getNextMatrix(state, action, playerNumber);
	}

	@Override
	public boolean equals(Object anotherSAP) {
		if (!(anotherSAP instanceof StateActionPair)) {
			return false;
		}
		StateActionPair actualSAP = (StateActionPair) anotherSAP;
		if (this == anotherSAP) {
			return true;
		}
		if (Arrays.deepEquals(this.state, actualSAP.getState()) && this.action == actualSAP.getAction()
				&& this.playerNumber == actualSAP.getPlayerNumber()) {
			return true;
		}

		return false;
	}

	/**
	 * Gets the state associated with the state-action pair.
	 *
	 * @return int matrix. Entry key: 0 means empty, 1 means player 1, 2 means
	 *         player 2.
	 */
	public int[][] getState() {
		return state;
	}

	/**
	 * Gets the column that was played after that state.
	 *
	 * @return column number
	 */
	public int getAction() {
		return action;
	}

	/**
	 * Gets the player responsible for the state-action.
	 *
	 * @return player number
	 */
	public int getPlayerNumber() {
		return playerNumber;
	}

	/**
	 * Gets the state reached after the performed action. If the move was an illegal
	 * one, then it should be the same as the current state.
	 *
	 * @return int matrix after the action is made. Entry key: 0 means empty, 1
	 *         means player 1, 2 means player 2
	 */
	public int[][] getFutureState() {
		return futureState;
	}

}
