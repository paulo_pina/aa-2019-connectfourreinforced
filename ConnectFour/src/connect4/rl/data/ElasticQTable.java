package connect4.rl.data;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Defines an "elastic Q-Table". It saves tuples of game states/player states
 * and weights (standard Doubles).<br>
 *
 * Be aware that a q-table for player 1 will be inherently different to player
 * 2's q-table because of the asymmetry associated with turn-based gameplay!
 *
 * @author grovy
 *
 */
public class ElasticQTable implements Serializable {

	private static final long serialVersionUID = 401191528460077135L;
	private HashMap<SAPStateTuple, Double> weights;
	private final int playerNumber;

	public ElasticQTable(int playerNumber) {
		validatePlayerNumber(playerNumber);
		this.weights = new HashMap<SAPStateTuple, Double>();
		this.playerNumber = playerNumber;
	}

	public ElasticQTable(HashMap<SAPStateTuple, Double> weights, int playerNumber) {
		validatePlayerNumber(playerNumber);
		this.weights = weights;
		this.playerNumber = playerNumber;
	}

	private void validatePlayerNumber(int playerNumber) {
		if ((playerNumber != 1) && (playerNumber != 2)) {
			throw new IllegalArgumentException("ERROR: Invalid player number on qTable creation! " + playerNumber);
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof ElasticQTable)) {
			return false;
		}
		ElasticQTable otherQTable = (ElasticQTable) obj;
		if (this == otherQTable) {
			return true;
		}
		if (this.weights.equals(otherQTable.getWeights())) {
			return true;
		}
		return false;
	}

	/**
	 * Grabs the entire HashMap from this Elastic QTable. Be careful with this
	 * @return
	 */
	public HashMap<SAPStateTuple, Double> getWeights(){
		return this.weights;
	}

	/**
	 * Associates a single State-action pair to a weight. This weight should reflect
	 * the usefulness of that State-action pair.
	 *
	 * @param stateAction
	 * @param weight
	 */
	public void putWeight(SAPStateTuple stateAction, double weight) {
		this.weights.put(stateAction, weight);
	}

	/**
	 * Grabs a weight from a potentially existing State-action pair from the weights
	 * map. If no weight is associated with that pair, then it returns 0.
	 *
	 * @param stateAction
	 * @return a double that reflects the State-action pair's usefulness
	 */
	public double getWeight(SAPStateTuple stateAction) {
		if (this.weights.get(stateAction) == null) { // not yet in qTable
			return 0.0;
		}
		return this.weights.get(stateAction);
	}

	/**
	 * Gets the player number associated to this qTable.
	 *
	 * @return player number
	 */
	public int getPlayerNumber() {
		return playerNumber;
	}

}
