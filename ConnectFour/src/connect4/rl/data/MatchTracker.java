package connect4.rl.data;

import java.util.HashMap;

/**
 * Keeps track of a match's statistics.
 * @author grovy
 *
 */
public class MatchTracker {

	public MatchTracker() {
		// TODO Auto-generated constructor stub
	}

	public HashMap<Integer, Integer> updateColumnTimesPlayed(HashMap<Integer, Integer> playerColumnTimesPlayedInAllMatches, HashMap<Integer, Integer> columnPlayCountInOneMatch) {
		for (Integer column : columnPlayCountInOneMatch.keySet()) {
			int currentTimesPlayed = 0;
			int allTimesPlayed = 0;

			if (columnPlayCountInOneMatch.get(column) != null) {
				currentTimesPlayed = columnPlayCountInOneMatch.get(column);
			}
			if (playerColumnTimesPlayedInAllMatches.get(column) != null) {
				allTimesPlayed = playerColumnTimesPlayedInAllMatches.get(column);
			}
			allTimesPlayed += currentTimesPlayed;
			playerColumnTimesPlayedInAllMatches.put(column, allTimesPlayed);
		}
		return playerColumnTimesPlayedInAllMatches;

	}

	public void printVictoriesAndColumns(int matchNumber, int player1VictoryCount, int player2VictoryCount, HashMap<Integer, Integer> player1ColumnTimesPlayedInAllMatches, HashMap<Integer, Integer> player2ColumnTimesPlayedInAllMatches) {
		System.out.println("Matches: " + matchNumber);
		System.out.println("Player 1 victories: " + player1VictoryCount);
		System.out.println("Player 2 victories: " + player2VictoryCount);
		System.out.println("Times column played:\t0\t1\t2\t3\t4\t5\t6");
		System.out.println("P1:\t\t\t" + player1ColumnTimesPlayedInAllMatches.get(0) + "\t"
				+ player1ColumnTimesPlayedInAllMatches.get(1) + "\t"
				+ player1ColumnTimesPlayedInAllMatches.get(2) + "\t"
				+ player1ColumnTimesPlayedInAllMatches.get(3) + "\t"
				+ player1ColumnTimesPlayedInAllMatches.get(4) + "\t"
				+ player1ColumnTimesPlayedInAllMatches.get(5) + "\t"
				+ player1ColumnTimesPlayedInAllMatches.get(6) + "\t");
		System.out.println("P2:\t\t\t" + player2ColumnTimesPlayedInAllMatches.get(0) + "\t"
				+ player2ColumnTimesPlayedInAllMatches.get(1) + "\t"
				+ player2ColumnTimesPlayedInAllMatches.get(2) + "\t"
				+ player2ColumnTimesPlayedInAllMatches.get(3) + "\t"
				+ player2ColumnTimesPlayedInAllMatches.get(4) + "\t"
				+ player2ColumnTimesPlayedInAllMatches.get(5) + "\t"
				+ player2ColumnTimesPlayedInAllMatches.get(6) + "\t");
	}

}
