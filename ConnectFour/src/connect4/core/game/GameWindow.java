package connect4.core.game;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import connect4.core.logic.GameStateConsequence;
import connect4.core.logic.PlayerState;
import connect4.core.logic.StateChecker;
import connect4.rl.data.SAPStateTuple;
import connect4.rl.data.StateActionPair;

public class GameWindow extends JPanel {

	/**
	 *
	 */
	private static final long serialVersionUID = 5676071282262892305L;

	private Image icon1 = new ImageIcon("img/Jogador1.png").getImage().getScaledInstance(60, 60,
			java.awt.Image.SCALE_SMOOTH);
	private Image icon2 = new ImageIcon("img/Jogador2.png").getImage().getScaledInstance(60, 60,
			java.awt.Image.SCALE_SMOOTH);
	private Image move = icon1;

	private String mode;

	private int player = 1;
	private int matrix[][] = new int[6][7];
	private int turnCounter = 1;
	private SAPStateTuple lastWitnessedMove;

	private JLabel gameMode = new JLabel("MODE");
	private JLabel lastMoveTitle = new JLabel("Last Move:");
	private JLabel lastMove = new JLabel("");
	private JLabel stateTitle = new JLabel("State:");
	private JLabel state = new JLabel("Player 1, it's your turn to play");
	private JLabel player1 = new JLabel("Player 1");
	private JLabel player1Icon = new JLabel("");
	private JLabel player2 = new JLabel("Player 2");
	private JLabel player2Icon = new JLabel("");

	private JButton columnA = new JButton("A/0");
	private JButton columnB = new JButton("B/1");
	private JButton columnC = new JButton("C/2");
	private JButton columnD = new JButton("D/3");
	private JButton columnE = new JButton("E/4");
	private JButton columnF = new JButton("F/5");
	private JButton columnG = new JButton("G/6");

	private JLabel a0 = new JLabel("");
	private JLabel a1 = new JLabel("");
	private JLabel a2 = new JLabel("");
	private JLabel a3 = new JLabel("");
	private JLabel a4 = new JLabel("");
	private JLabel a5 = new JLabel("");

	private JLabel b0 = new JLabel("");
	private JLabel b1 = new JLabel("");
	private JLabel b2 = new JLabel("");
	private JLabel b3 = new JLabel("");
	private JLabel b4 = new JLabel("");
	private JLabel b5 = new JLabel("");

	private JLabel c0 = new JLabel("");
	private JLabel c1 = new JLabel("");
	private JLabel c2 = new JLabel("");
	private JLabel c3 = new JLabel("");
	private JLabel c4 = new JLabel("");
	private JLabel c5 = new JLabel("");

	private JLabel d0 = new JLabel("");
	private JLabel d1 = new JLabel("");
	private JLabel d2 = new JLabel("");
	private JLabel d3 = new JLabel("");
	private JLabel d4 = new JLabel("");
	private JLabel d5 = new JLabel("");

	private JLabel e0 = new JLabel("");
	private JLabel e1 = new JLabel("");
	private JLabel e2 = new JLabel("");
	private JLabel e3 = new JLabel("");
	private JLabel e4 = new JLabel("");
	private JLabel e5 = new JLabel("");

	private JLabel f0 = new JLabel("");
	private JLabel f1 = new JLabel("");
	private JLabel f2 = new JLabel("");
	private JLabel f3 = new JLabel("");
	private JLabel f4 = new JLabel("");
	private JLabel f5 = new JLabel("");

	private JLabel g0 = new JLabel("");
	private JLabel g1 = new JLabel("");
	private JLabel g2 = new JLabel("");
	private JLabel g3 = new JLabel("");
	private JLabel g4 = new JLabel("");
	private JLabel g5 = new JLabel("");

	public GameWindow(String mode) {

		this.mode = mode;
		setBackground(Color.DARK_GRAY);
		this.setBounds(100, 100, 505, 580);
		setLayout(null);

		// FIXME we can probably do without the numerous "void add(Component)" calls if
		// the game's on a LEARNing phase.
		gameMode.setFont(new Font("Tahoma", Font.PLAIN, 18));
		gameMode.setForeground(Color.WHITE);
		gameMode.setHorizontalAlignment(SwingConstants.CENTER);
		gameMode.setBounds(25, 13, 450, 30);
		gameMode.setText(mode + " MODE");
		add(gameMode);

		lastMoveTitle.setForeground(Color.WHITE);
		lastMoveTitle.setFont(new Font("Tahoma", Font.BOLD, 14));
		lastMoveTitle.setBounds(25, 60, 80, 30);
		add(lastMoveTitle);

		lastMove.setForeground(Color.WHITE);
		lastMove.setFont(new Font("Tahoma", Font.ITALIC, 14));
		lastMove.setBounds(110, 60, 235, 30);
		add(lastMove);

		stateTitle.setForeground(Color.WHITE);
		stateTitle.setFont(new Font("Tahoma", Font.BOLD, 14));
		stateTitle.setBounds(25, 90, 45, 30);
		add(stateTitle);

		state.setForeground(Color.WHITE);
		state.setFont(new Font("Tahoma", Font.ITALIC, 14));
		state.setBounds(75, 90, 270, 30);
		add(state);

		player1.setHorizontalAlignment(SwingConstants.RIGHT);
		player1.setForeground(Color.WHITE);
		player1.setFont(new Font("Tahoma", Font.BOLD, 14));
		player1.setBounds(350, 60, 88, 30);
		add(player1);

		player1Icon.setHorizontalAlignment(SwingConstants.RIGHT);
		player1Icon.setForeground(Color.WHITE);
		player1Icon.setFont(new Font("Tahoma", Font.BOLD, 14));
		player1Icon.setBounds(450, 60, 25, 30);
		add(player1Icon);
		player1Icon.setIcon(new ImageIcon(icon1.getScaledInstance(25, 25, java.awt.Image.SCALE_SMOOTH)));

		player2.setHorizontalAlignment(SwingConstants.RIGHT);
		player2.setForeground(Color.WHITE);
		player2.setFont(new Font("Tahoma", Font.BOLD, 14));
		player2.setBounds(350, 90, 88, 30);
		add(player2);
		if (mode == "SINGLEPLAYER") // FIXME only add stuff to the JPanel if it's SINGLEPLAYER. Maybe.
			player2.setText("Computer");

		player2Icon.setHorizontalAlignment(SwingConstants.RIGHT);
		player2Icon.setForeground(Color.WHITE);
		player2Icon.setFont(new Font("Tahoma", Font.BOLD, 14));
		player2Icon.setBounds(450, 90, 25, 30);
		add(player2Icon);
		player2Icon.setIcon(new ImageIcon(icon2.getScaledInstance(25, 25, java.awt.Image.SCALE_SMOOTH)));

		// COLUMN A

		a0.setOpaque(true);
		a0.setBackground(Color.LIGHT_GRAY);
		a0.setBounds(25, 495, 60, 60);
		add(a0);

		a1.setOpaque(true);
		a1.setBackground(Color.LIGHT_GRAY);
		a1.setBounds(25, 430, 60, 60);
		add(a1);

		a2.setOpaque(true);
		a2.setBackground(Color.LIGHT_GRAY);
		a2.setBounds(25, 365, 60, 60);
		add(a2);

		a3.setOpaque(true);
		a3.setBackground(Color.LIGHT_GRAY);
		a3.setBounds(25, 300, 60, 60);
		add(a3);

		a4.setOpaque(true);
		a4.setBackground(Color.LIGHT_GRAY);
		a4.setBounds(25, 235, 60, 60);
		add(a4);

		a5.setOpaque(true);
		a5.setBackground(Color.LIGHT_GRAY);
		a5.setBounds(25, 170, 60, 60);
		add(a5);

		// COLUMN B

		b0.setOpaque(true);
		b0.setBackground(Color.LIGHT_GRAY);
		b0.setBounds(90, 495, 60, 60);
		add(b0);

		b1.setOpaque(true);
		b1.setBackground(Color.LIGHT_GRAY);
		b1.setBounds(90, 430, 60, 60);
		add(b1);

		b2.setOpaque(true);
		b2.setBackground(Color.LIGHT_GRAY);
		b2.setBounds(90, 365, 60, 60);
		add(b2);

		b3.setOpaque(true);
		b3.setBackground(Color.LIGHT_GRAY);
		b3.setBounds(90, 300, 60, 60);
		add(b3);

		b4.setOpaque(true);
		b4.setBackground(Color.LIGHT_GRAY);
		b4.setBounds(90, 235, 60, 60);
		add(b4);

		b5.setOpaque(true);
		b5.setBackground(Color.LIGHT_GRAY);
		b5.setBounds(90, 170, 60, 60);
		add(b5);

		// COLUMN C

		c0.setOpaque(true);
		c0.setBackground(Color.LIGHT_GRAY);
		c0.setBounds(155, 495, 60, 60);
		add(c0);

		c1.setOpaque(true);
		c1.setBackground(Color.LIGHT_GRAY);
		c1.setBounds(155, 430, 60, 60);
		add(c1);

		c2.setOpaque(true);
		c2.setBackground(Color.LIGHT_GRAY);
		c2.setBounds(155, 365, 60, 60);
		add(c2);

		c3.setOpaque(true);
		c3.setBackground(Color.LIGHT_GRAY);
		c3.setBounds(155, 300, 60, 60);
		add(c3);

		c4.setOpaque(true);
		c4.setBackground(Color.LIGHT_GRAY);
		c4.setBounds(155, 235, 60, 60);
		add(c4);

		c5.setOpaque(true);
		c5.setBackground(Color.LIGHT_GRAY);
		c5.setBounds(155, 170, 60, 60);
		add(c5);

		// COLUMN D

		d0.setOpaque(true);
		d0.setBackground(Color.LIGHT_GRAY);
		d0.setBounds(220, 495, 60, 60);
		add(d0);

		d1.setOpaque(true);
		d1.setBackground(Color.LIGHT_GRAY);
		d1.setBounds(220, 430, 60, 60);
		add(d1);

		d2.setOpaque(true);
		d2.setBackground(Color.LIGHT_GRAY);
		d2.setBounds(220, 365, 60, 60);
		add(d2);

		d3.setOpaque(true);
		d3.setBackground(Color.LIGHT_GRAY);
		d3.setBounds(220, 300, 60, 60);
		add(d3);

		d4.setOpaque(true);
		d4.setBackground(Color.LIGHT_GRAY);
		d4.setBounds(220, 235, 60, 60);
		add(d4);

		d5.setOpaque(true);
		d5.setBackground(Color.LIGHT_GRAY);
		d5.setBounds(220, 170, 60, 60);
		add(d5);

		// COLUMN E

		e0.setOpaque(true);
		e0.setBackground(Color.LIGHT_GRAY);
		e0.setBounds(285, 495, 60, 60);
		add(e0);

		e1.setOpaque(true);
		e1.setBackground(Color.LIGHT_GRAY);
		e1.setBounds(285, 430, 60, 60);
		add(e1);

		e2.setOpaque(true);
		e2.setBackground(Color.LIGHT_GRAY);
		e2.setBounds(285, 365, 60, 60);
		add(e2);

		e3.setOpaque(true);
		e3.setBackground(Color.LIGHT_GRAY);
		e3.setBounds(285, 300, 60, 60);
		add(e3);

		e4.setOpaque(true);
		e4.setBackground(Color.LIGHT_GRAY);
		e4.setBounds(285, 235, 60, 60);
		add(e4);

		e5.setOpaque(true);
		e5.setBackground(Color.LIGHT_GRAY);
		e5.setBounds(285, 170, 60, 60);
		add(e5);

		// COLUMN F

		f0.setOpaque(true);
		f0.setBackground(Color.LIGHT_GRAY);
		f0.setBounds(350, 495, 60, 60);
		add(f0);

		f1.setOpaque(true);
		f1.setBackground(Color.LIGHT_GRAY);
		f1.setBounds(350, 430, 60, 60);
		add(f1);

		f2.setOpaque(true);
		f2.setBackground(Color.LIGHT_GRAY);
		f2.setBounds(350, 365, 60, 60);
		add(f2);

		f3.setOpaque(true);
		f3.setBackground(Color.LIGHT_GRAY);
		f3.setBounds(350, 300, 60, 60);
		add(f3);

		f4.setOpaque(true);
		f4.setBackground(Color.LIGHT_GRAY);
		f4.setBounds(350, 235, 60, 60);
		add(f4);

		f5.setOpaque(true);
		f5.setBackground(Color.LIGHT_GRAY);
		f5.setBounds(350, 170, 60, 60);
		add(f5);

		// COLUMN G

		g0.setOpaque(true);
		g0.setBackground(Color.LIGHT_GRAY);
		g0.setBounds(415, 495, 60, 60);
		add(g0);

		g1.setOpaque(true);
		g1.setBackground(Color.LIGHT_GRAY);
		g1.setBounds(415, 430, 60, 60);
		add(g1);

		g2.setOpaque(true);
		g2.setBackground(Color.LIGHT_GRAY);
		g2.setBounds(415, 365, 60, 60);
		add(g2);

		g3.setOpaque(true);
		g3.setBackground(Color.LIGHT_GRAY);
		g3.setBounds(415, 300, 60, 60);
		add(g3);

		g4.setOpaque(true);
		g4.setBackground(Color.LIGHT_GRAY);
		g4.setBounds(415, 235, 60, 60);
		add(g4);

		g5.setOpaque(true);
		g5.setBackground(Color.LIGHT_GRAY);
		g5.setBounds(415, 170, 60, 60);
		add(g5);

		// COLUMNS BUTTONS

		columnA.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				addPieceToColumn("A");
			}
		});
		columnA.setBounds(25, 145, 60, 19);
		add(columnA);

		columnB.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				addPieceToColumn("B");
			}
		});
		columnB.setBounds(90, 145, 60, 19);
		add(columnB);

		columnC.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				addPieceToColumn("C");
			}
		});
		columnC.setBounds(155, 145, 60, 19);
		add(columnC);

		columnD.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				addPieceToColumn("D");
			}
		});
		columnD.setBounds(220, 145, 60, 19);
		add(columnD);

		columnE.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				addPieceToColumn("E");
			}
		});
		columnE.setBounds(285, 145, 60, 19);
		add(columnE);

		columnF.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				addPieceToColumn("F");
			}
		});
		columnF.setBounds(350, 145, 60, 19);
		add(columnF);

		columnG.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				addPieceToColumn("G");
			}
		});
		columnG.setBounds(415, 145, 60, 19);
		add(columnG);
	}

	public SAPStateTuple addPieceToColumn(String column) {
		int selectedColumn = -1;
		StateActionPair sap = null;
		switch (column) {
		case "A":
			selectedColumn = 0;
			sap = new StateActionPair(this.matrix, selectedColumn, this.player);
			if (this.columnA.isEnabled() == false) {
				System.out.println("Player " + this.player + " attempted an illegal move on column A on turn "
						+ this.turnCounter + "!");
				PlayerState ps = new PlayerState(GameStateConsequence.ILLEGAL_MOVE, this.player);
				return new SAPStateTuple(sap, ps);
			}
			if (this.a0.getIcon() == null) {
				this.a0.setIcon(new ImageIcon(this.move));
				this.matrix[0][0] = this.player;
			} else if (this.a1.getIcon() == null) {
				this.a1.setIcon(new ImageIcon(this.move));
				this.matrix[1][0] = this.player;
			} else if (this.a2.getIcon() == null) {
				this.a2.setIcon(new ImageIcon(this.move));
				this.matrix[2][0] = this.player;
			} else if (this.a3.getIcon() == null) {
				this.a3.setIcon(new ImageIcon(this.move));
				this.matrix[3][0] = this.player;
			} else if (this.a4.getIcon() == null) {
				this.a4.setIcon(new ImageIcon(this.move));
				this.matrix[4][0] = this.player;
			} else if (this.a5.getIcon() == null) {
				this.a5.setIcon(new ImageIcon(this.move));
				this.matrix[5][0] = this.player;
				this.columnA.setEnabled(false);
			}
			this.lastMove.setText("Column A");
			break;
		case "B":
			selectedColumn = 1;
			sap = new StateActionPair(this.matrix, selectedColumn, this.player);
			if (this.columnB.isEnabled() == false) {
				System.out.println("Player " + this.player + " attempted an illegal move on column B on turn "
						+ this.turnCounter + "!");
				PlayerState ps = new PlayerState(GameStateConsequence.ILLEGAL_MOVE, this.player);
				return new SAPStateTuple(sap, ps);
			}
			if (this.b0.getIcon() == null) {
				this.b0.setIcon(new ImageIcon(this.move));
				this.matrix[0][1] = this.player;
			} else if (this.b1.getIcon() == null) {
				this.b1.setIcon(new ImageIcon(this.move));
				this.matrix[1][1] = this.player;
			} else if (this.b2.getIcon() == null) {
				this.b2.setIcon(new ImageIcon(this.move));
				this.matrix[2][1] = this.player;
			} else if (this.b3.getIcon() == null) {
				this.b3.setIcon(new ImageIcon(this.move));
				this.matrix[3][1] = this.player;
			} else if (this.b4.getIcon() == null) {
				this.b4.setIcon(new ImageIcon(this.move));
				this.matrix[4][1] = this.player;
			} else if (this.b5.getIcon() == null) {
				this.b5.setIcon(new ImageIcon(this.move));
				this.matrix[5][1] = this.player;
				this.columnB.setEnabled(false);
			}
			this.lastMove.setText("Column B");
			break;

		case "C":
			selectedColumn = 2;
			sap = new StateActionPair(this.matrix, selectedColumn, this.player);
			if (this.columnC.isEnabled() == false) {
				System.out.println("Player " + this.player + " attempted an illegal move on column C on turn "
						+ this.turnCounter + "!");
				PlayerState ps = new PlayerState(GameStateConsequence.ILLEGAL_MOVE, this.player);
				return new SAPStateTuple(sap, ps);
			}
			if (this.c0.getIcon() == null) {
				this.c0.setIcon(new ImageIcon(this.move));
				this.matrix[0][2] = this.player;
			} else if (this.c1.getIcon() == null) {
				this.c1.setIcon(new ImageIcon(this.move));
				this.matrix[1][2] = this.player;
			} else if (this.c2.getIcon() == null) {
				this.c2.setIcon(new ImageIcon(this.move));
				this.matrix[2][2] = this.player;
			} else if (this.c3.getIcon() == null) {
				this.c3.setIcon(new ImageIcon(this.move));
				this.matrix[3][2] = this.player;
			} else if (this.c4.getIcon() == null) {
				this.c4.setIcon(new ImageIcon(this.move));
				this.matrix[4][2] = this.player;
			} else if (this.c5.getIcon() == null) {
				this.c5.setIcon(new ImageIcon(this.move));
				this.matrix[5][2] = this.player;
				this.columnC.setEnabled(false);
			}
			this.lastMove.setText("Column C");
			break;

		case "D":
			selectedColumn = 3;
			sap = new StateActionPair(this.matrix, selectedColumn, this.player);
			if (this.columnD.isEnabled() == false) {
				System.out.println("Player " + this.player + " attempted an illegal move on column D on turn "
						+ this.turnCounter + "!");
				PlayerState ps = new PlayerState(GameStateConsequence.ILLEGAL_MOVE, this.player);
				return new SAPStateTuple(sap, ps);
			}
			if (this.d0.getIcon() == null) {
				this.d0.setIcon(new ImageIcon(this.move));
				this.matrix[0][3] = this.player;
			} else if (this.d1.getIcon() == null) {
				this.d1.setIcon(new ImageIcon(this.move));
				this.matrix[1][3] = this.player;
			} else if (this.d2.getIcon() == null) {
				this.d2.setIcon(new ImageIcon(this.move));
				this.matrix[2][3] = this.player;
			} else if (this.d3.getIcon() == null) {
				this.d3.setIcon(new ImageIcon(this.move));
				this.matrix[3][3] = this.player;
			} else if (this.d4.getIcon() == null) {
				this.d4.setIcon(new ImageIcon(this.move));
				this.matrix[4][3] = this.player;
			} else if (this.d5.getIcon() == null) {
				this.d5.setIcon(new ImageIcon(this.move));
				this.matrix[5][3] = this.player;
				this.columnD.setEnabled(false);
			}
			this.lastMove.setText("Column D");
			break;

		case "E":
			selectedColumn = 4;
			sap = new StateActionPair(this.matrix, selectedColumn, this.player);
			if (this.columnE.isEnabled() == false) {
				System.out.println("Player " + this.player + " attempted an illegal move on column E on turn "
						+ this.turnCounter + "!");
				PlayerState ps = new PlayerState(GameStateConsequence.ILLEGAL_MOVE, this.player);
				return new SAPStateTuple(sap, ps);
			}
			if (this.e0.getIcon() == null) {
				this.e0.setIcon(new ImageIcon(this.move));
				this.matrix[0][4] = this.player;
			} else if (this.e1.getIcon() == null) {
				this.e1.setIcon(new ImageIcon(this.move));
				this.matrix[1][4] = this.player;
			} else if (this.e2.getIcon() == null) {
				this.e2.setIcon(new ImageIcon(this.move));
				this.matrix[2][4] = this.player;
			} else if (this.e3.getIcon() == null) {
				this.e3.setIcon(new ImageIcon(this.move));
				this.matrix[3][4] = this.player;
			} else if (this.e4.getIcon() == null) {
				this.e4.setIcon(new ImageIcon(this.move));
				this.matrix[4][4] = this.player;
			} else if (this.e5.getIcon() == null) {
				this.e5.setIcon(new ImageIcon(this.move));
				this.matrix[5][4] = this.player;
				this.columnE.setEnabled(false);
			}
			this.lastMove.setText("Column E");
			break;

		case "F":
			selectedColumn = 5;
			sap = new StateActionPair(this.matrix, selectedColumn, this.player);
			if (this.columnF.isEnabled() == false) {
				System.out.println("Player " + this.player + " attempted an illegal move on column F on turn "
						+ this.turnCounter + "!");
				PlayerState ps = new PlayerState(GameStateConsequence.ILLEGAL_MOVE, this.player);
				return new SAPStateTuple(sap, ps);
			}
			if (this.f0.getIcon() == null) {
				this.f0.setIcon(new ImageIcon(this.move));
				this.matrix[0][5] = this.player;
			} else if (this.f1.getIcon() == null) {
				this.f1.setIcon(new ImageIcon(this.move));
				this.matrix[1][5] = this.player;
			} else if (this.f2.getIcon() == null) {
				this.f2.setIcon(new ImageIcon(this.move));
				this.matrix[2][5] = this.player;
			} else if (this.f3.getIcon() == null) {
				this.f3.setIcon(new ImageIcon(this.move));
				this.matrix[3][5] = this.player;
			} else if (this.f4.getIcon() == null) {
				this.f4.setIcon(new ImageIcon(this.move));
				this.matrix[4][5] = this.player;
			} else if (this.f5.getIcon() == null) {
				this.f5.setIcon(new ImageIcon(this.move));
				this.matrix[5][5] = this.player;
				this.columnF.setEnabled(false);
			}
			this.lastMove.setText("Column F");
			break;

		case "G":
			selectedColumn = 6;
			sap = new StateActionPair(this.matrix, selectedColumn, this.player);
			if (this.columnG.isEnabled() == false) {
				System.out.println("Player " + this.player + " attempted an illegal move on column G on turn "
						+ this.turnCounter + "!");
				PlayerState ps = new PlayerState(GameStateConsequence.ILLEGAL_MOVE, this.player);
				return new SAPStateTuple(sap, ps);
			}
			if (this.g0.getIcon() == null) {
				this.g0.setIcon(new ImageIcon(this.move));
				this.matrix[0][6] = this.player;
			} else if (this.g1.getIcon() == null) {
				this.g1.setIcon(new ImageIcon(this.move));
				this.matrix[1][6] = this.player;
			} else if (this.g2.getIcon() == null) {
				this.g2.setIcon(new ImageIcon(this.move));
				this.matrix[2][6] = this.player;
			} else if (this.g3.getIcon() == null) {
				this.g3.setIcon(new ImageIcon(this.move));
				this.matrix[3][6] = this.player;
			} else if (this.g4.getIcon() == null) {
				this.g4.setIcon(new ImageIcon(this.move));
				this.matrix[4][6] = this.player;
			} else if (this.g5.getIcon() == null) {
				this.g5.setIcon(new ImageIcon(this.move));
				this.matrix[5][6] = this.player;
				this.columnG.setEnabled(false);
			}
			this.lastMove.setText("Column G");
			break;
		}

		Enum<GameStateConsequence> newState = StateChecker.verifyGameState(this.player, this.matrix);
		PlayerState ps = new PlayerState(newState, this.player);
		boolean countTurn = true;
		if ((newState != GameStateConsequence.CONTINUE) || (newState == GameStateConsequence.ILLEGAL_MOVE)) {
			this.columnA.setEnabled(false);
			this.columnB.setEnabled(false);
			this.columnC.setEnabled(false);
			this.columnD.setEnabled(false);
			this.columnE.setEnabled(false);
			this.columnF.setEnabled(false);
			this.columnG.setEnabled(false);
			countTurn = false;
		}

		if (newState == GameStateConsequence.VICTORY) {
			this.state.setText("Player " + player + " won the game.");
			if (mode.equals("SINGLEPLAYER")) {
				JOptionPane.showMessageDialog(null, "Player " + player + " won the game.");
			}
		} else if (newState == GameStateConsequence.DRAW) {
			this.state.setText("The game ended in a draw");
			if (mode.equals("SINGLEPLAYER")) {
				JOptionPane.showMessageDialog(null, "The game ended in a draw");
			}
		} else {
			if (this.move == this.icon1) {
				this.move = this.icon2;
				this.player = 2;

				if (mode.equals("MULTIPLAYER")) // FIXME this is no longer relevant, we need to change behaviour to
												// support
					// LEARNing.
					this.state.setText("Player 2, it's your turn to play");
				else
					this.state.setText("Computer is thinking ...");
			} else {
				this.move = this.icon1;
				this.player = 1;
				this.state.setText("Player 1, it's your turn to play");
			}
		}
		if (countTurn) {
			this.turnCounter++;
		}
		if (selectedColumn == -1) {
			throw new IllegalStateException("ERROR: selectedColumn on GameWindow is " + selectedColumn + "!!!! PANIC");
		}

		this.lastWitnessedMove = new SAPStateTuple(sap, ps);
		return this.lastWitnessedMove;
	}

	public int getCurrentPlayer() {
		return this.player;
	}

	public int getTurnCount() {
		return this.turnCounter;
	}

	public int[][] getCurrentMatrix() {
		return this.matrix;
	}

	public SAPStateTuple getLastWitnessedMove() {
		return this.lastWitnessedMove;
	}

}
