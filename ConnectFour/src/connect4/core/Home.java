package connect4.core;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class Home extends JPanel {

	/**
	 *
	 */
	private static final long serialVersionUID = 2948411969034947636L;
	private Image ISCTElogo = new ImageIcon("img/ISCTELogo.png").getImage().getScaledInstance(481, 134, java.awt.Image.SCALE_SMOOTH);

	public Home() {
		setBackground(Color.WHITE);
		this.setBounds(100, 100, 505, 580);
		setLayout(null);

		JLabel iscteLogo = new JLabel("");
		iscteLogo.setBounds(12, 54, 481, 134);
		add(iscteLogo);
		iscteLogo.setIcon(new ImageIcon(ISCTElogo));

		JLabel title = new JLabel("MACHINE LEARNING");
		title.setForeground(new Color(0, 102, 153));
		title.setFont(new Font("Tahoma", Font.BOLD, 24));
		title.setHorizontalAlignment(SwingConstants.CENTER);
		title.setBounds(12, 276, 481, 34);
		add(title);

		JLabel subtitle = new JLabel("Connect Four");
		subtitle.setForeground(new Color(0, 0, 102));
		subtitle.setHorizontalAlignment(SwingConstants.CENTER);
		subtitle.setFont(new Font("Calibri", Font.ITALIC, 22));
		subtitle.setBounds(12, 312, 481, 34);
		add(subtitle);

		JLabel author1 = new JLabel("Paulo Vitor de Pina - 73324");
		author1.setForeground(Color.DARK_GRAY);
		author1.setHorizontalAlignment(SwingConstants.CENTER);
		author1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		author1.setBounds(12, 442, 481, 25);
		add(author1);

		JLabel author2 = new JLabel("Rodolfo Farinha - 88812");
		author2.setForeground(Color.DARK_GRAY);
		author2.setHorizontalAlignment(SwingConstants.CENTER);
		author2.setFont(new Font("Tahoma", Font.PLAIN, 16));
		author2.setBounds(12, 470, 481, 25);
		add(author2);
	}
}
