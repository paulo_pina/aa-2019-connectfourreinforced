package connect4.core;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import connect4.core.game.GameWindow;

public class ConnectFour {

	private JFrame frame;
	private String iconPath = "img/IconApp.png";
	private Image iconApp = new ImageIcon(iconPath).getImage();

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					ConnectFour window = new ConnectFour();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public ConnectFour() {
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 505, 635);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Connect Four");
		frame.setIconImage(iconApp);
		frame.setLocationRelativeTo(null);

		JPanel panel = new Home();
		frame.getContentPane().removeAll();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		frame.revalidate();

		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);

		JMenu mnMenu = new JMenu("Menu");
		mnMenu.setFont(new Font("Calibri", Font.PLAIN, 18));
		menuBar.add(mnMenu);

		JMenu mnNewGame = new JMenu("New Game");
		mnNewGame.setFont(new Font("Calibri", Font.PLAIN, 18));
		mnMenu.add(mnNewGame);

		JMenuItem mntmSingleplayer = new JMenuItem("Singleplayer");
		mntmSingleplayer.setFont(new Font("Calibri", Font.PLAIN, 18));
		mntmSingleplayer.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {

				int response = JOptionPane.showConfirmDialog(null, "Do you want play a new game?", "Confirm",
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

				if (response == JOptionPane.YES_OPTION) {
					JPanel panel = new GameWindow("SINGLEPLAYER");
					frame.getContentPane().removeAll();
					frame.getContentPane().add(panel, BorderLayout.CENTER);
					frame.revalidate();
				}
			}
		});
		mnNewGame.add(mntmSingleplayer);

		JMenuItem mntmLearn = new JMenuItem("Learn");
		mntmLearn.setFont(new Font("Calibri", Font.PLAIN, 18));
		mntmLearn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				int response = JOptionPane.showConfirmDialog(null,
						"This will start creating a new ML model for the game. Proceed?", "Confirm",
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

				if (response == JOptionPane.YES_OPTION) {
					JPanel panel = new GameWindow("LEARNING");
					frame.getContentPane().removeAll();
					frame.getContentPane().add(panel, BorderLayout.CENTER);
					frame.revalidate();
				}
			}
		});
		mnNewGame.add(mntmLearn);

		JMenuItem mntmOptions = new JMenuItem("Options");
		mntmOptions.setFont(new Font("Calibri", Font.PLAIN, 18));
		mnMenu.add(mntmOptions);

		JMenuItem mntmHelp = new JMenuItem("Help");
		mntmHelp.setFont(new Font("Calibri", Font.PLAIN, 18));
		mnMenu.add(mntmHelp);

		JMenuItem mntmClose = new JMenuItem("Close");
		mntmClose.setFont(new Font("Calibri", Font.PLAIN, 18));
		mnMenu.add(mntmClose);
	}

}
