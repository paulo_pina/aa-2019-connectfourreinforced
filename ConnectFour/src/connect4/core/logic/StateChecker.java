package connect4.core.logic;

import java.util.ArrayList;

/**
 * Responsible for computing state differences
 *
 * @author grovy
 *
 */
public class StateChecker {

	/**
	 * Computes the current game state depending on the player who just completed
	 * his turn.
	 *
	 * @param player
	 * @param matrix
	 * @return
	 */
	public static Enum<GameStateConsequence> verifyGameState(int player, int matrix[][]) {
		// VERIFY CONNECT FOUR IN COLUMNS
		for (int column = 0; column < 7; column++) {
			for (int row = 0; row < 3; row++) {
				if (matrix[row][column] == player && matrix[row + 1][column] == player
						&& matrix[row + 2][column] == player && matrix[row + 3][column] == player) {
					return GameStateConsequence.VICTORY;
				}
			}
		}

		// VERIFY CONNECT FOUR IN ROWS
		for (int row = 0; row < 6; row++) {
			for (int column = 0; column < 4; column++) {
				if (matrix[row][column] == player && matrix[row][column + 1] == player
						&& matrix[row][column + 2] == player && matrix[row][column + 3] == player) {
					return GameStateConsequence.VICTORY;
				}
			}
		}

		// VERIFY CONNECT FOUR IN POSITIVE DIAGONALS
		for (int column = 0; column < 4; column++) {
			for (int row = 3; row < 6; row++) {
				if (matrix[row][column] == player && matrix[row - 1][column + 1] == player
						&& matrix[row - 2][column + 2] == player && matrix[row - 3][column + 3] == player) {
					return GameStateConsequence.VICTORY;
				}
			}
		}

		// VERIFY CONNECT FOUR IN NEGATIVE DIAGONALS
		for (int column = 0; column < 4; column++) {
			for (int row = 2; row > -1; row--) {
				if (matrix[row][column] == player && matrix[row + 1][column + 1] == player
						&& matrix[row + 2][column + 2] == player && matrix[row + 3][column + 3] == player) {
					return GameStateConsequence.VICTORY;
				}
			}
		}
		// CHECK DRAW
		for (int column = 0; column < 7; column++) {
			if ((matrix[5][column] != 1 && matrix[5][column] != 2)) {
				return GameStateConsequence.CONTINUE;
			}
		}
		return GameStateConsequence.DRAW;
	}

	/**
	 * Checks if a column is full by checking whether the topmost element of that
	 * column is filled.
	 *
	 * @param columnNumber
	 * @param matrix
	 * @return
	 */
	public static boolean isColumnFull(int columnNumber, int matrix[][]) {
		if (matrix[5][columnNumber] != 0) {
			return true;
		}
		return false;
	}

	/**
	 * Returns a list of available columns for play.
	 *
	 * @param currentMatrix
	 * @return
	 */
	public static ArrayList<Integer> getFreeColumns(int[][] currentMatrix) {
		int columnCount = currentMatrix[5].length;

		ArrayList<Integer> freeColumns = new ArrayList<Integer>();

		for (int i = 0; i < columnCount; i++) {
			boolean columnIsFree = !StateChecker.isColumnFull(i, currentMatrix);
			if (columnIsFree) {
				freeColumns.add(i);
			} else {
				continue;
			}
		}

		return freeColumns;
	}

	/**
	 * Returns the next valid matrix after a certain move by a player.
	 *
	 * @param matrix
	 * @param column
	 * @param playerNumber
	 * @return
	 */
	public static int[][] getNextMatrix(int[][] matrix, int column, int playerNumber) {
		int[][] result = new int[6][7];
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 7; j++) {
				result[i][j] = matrix[i][j];
			}
		}

		for (int i = 0; i < 6; i++) {
			if (matrix[i][column] == 0) {
				result[i][column] = playerNumber;
				break;
			}
		}

		return result;
	}

}
