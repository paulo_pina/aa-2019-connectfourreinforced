package connect4.core.logic;

/**
 * Defines the consequence of a game state. VICTORY and DRAW are distinct
 * halting states (on the former, somebody wins and the other loses, on the
 * latter, no one wins), CONTINUE passes the turn, and ILLEGAL_MOVE prohibits
 * the player's action and encourages them to pick another.
 *
 * @author grovy
 *
 */
public enum GameStateConsequence {

	VICTORY, DRAW, CONTINUE, ILLEGAL_MOVE

}
