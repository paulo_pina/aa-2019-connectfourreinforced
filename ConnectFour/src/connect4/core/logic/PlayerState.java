package connect4.core.logic;

import java.io.Serializable;

/**
 * Defines the consequence of a move, who did it and when.
 *
 * @author grovy
 *
 */
public final class PlayerState implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -3766972202713681263L;
	private final Enum<GameStateConsequence> stateConsequence;
	private final int playerNumber;

	public PlayerState(Enum<GameStateConsequence> state, int player) {
		this.stateConsequence = state;
		this.playerNumber = player;
	}

	@Override
	public boolean equals(Object anotherPlayerState) {
		if (!(anotherPlayerState instanceof PlayerState)) {
			return false;
		}
		PlayerState actualPlayerState = (PlayerState) anotherPlayerState;
		if (this == actualPlayerState) {
			return true;
		}
		if (this.stateConsequence.equals(actualPlayerState.getStateConsequence())
				&& this.playerNumber == actualPlayerState.getPlayerNumber()) {
			return true;
		}

		return false;
	}

	/**
	 * Gets the player responsible for the move.
	 *
	 * @return player number
	 */
	public int getPlayerNumber() {
		return this.playerNumber;
	}

	/**
	 * Gets the consequence of the current game state.
	 *
	 * @see GameStateConsequence
	 * @return Game State Consequence
	 */
	public Enum<GameStateConsequence> getStateConsequence() {
		return this.stateConsequence;
	}

}
